package ru.t1.aayakovlev.tm.constant;

public final class TerminalConstant {

    public final static String HELP = "help";

    public final static String ABOUT = "about";

    public final static String VERSION = "version";

    private TerminalConstant() {
    }

}
